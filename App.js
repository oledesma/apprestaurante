import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import LoginScreen from './screens/LoginScreen';
import Menu from './screens/Menu';
import Reservas from './screens/Reservas';
import Products from './screens/Products';
import ProductDetail from './screens/ProductDetail';
import Settings from './screens/Settings';
import Pedidos from './screens/Pedidos';
import NuevoPedido from './screens/NuevoPedido';
import FinalPedido from './screens/FinalPedido';

export default class App extends Component {
  render() {
    return (
      <View style={styles.loginContainer}>
        <FinalPedido />
      </View>
    );
  }
}

const ELEMENT_HEIGHT = 200;

class Productos extends Component {
  render() {
    return <Text>Hola</Text>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 200,
  },
  productosContainer: {
    flex: 1,
  },
  list: {
    alignItems: 'center',
    paddingTop: 20,
  },
  buttonContainer: {
    backgroundColor: '#ff8657',
    padding: 15,
    marginTop: 30,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
  },
  loginContainer: {
    flex: 1,
    height: '100%',
  },
  imageBackground: {
    flex: 1,
    opacity: 0.8,
  },
  logoContainer: {
    flex: 2,
    flexDirection: 'column',
  },
  botones: {
    flex: 1,
    backgroundColor: '#ff8657',
    margin: 5,
    borderRadius: 20,
    alignItems: 'center',
  },
  buttonsStyle: {
    flex: 4,
    marginLeft: 10,
    borderRadius: 20,
    width: 47,
  },
  background: {
    flex: 1,
  },
  button: {
    width: '40%',
    height: 40,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    borderRadius: 10,
  },
  separatorLine: {
    backgroundColor: '#fff',
    height: 2,
    width: '45%',
  },
  imageIconStyle: {
    marginTop: 15,
    height: 55,
    width: 50,
    resizeMode: 'stretch',
  },
});
