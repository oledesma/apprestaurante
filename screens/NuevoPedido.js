import React from "react";
import {FlatList, Image, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import {Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/AntDesign'
import SelectInput from 'react-native-select-input-ios';

const items = [
    // this is the parent or 'item'
    {
        name: 'Fruits',
        id: 0,
        // these are the children or 'sub items'
        children: [
            {
                name: 'Apple',
                id: 10,
            },
            {
                name: 'Strawberry',
                id: 17,
            },
            {
                name: 'Pineapple',
                id: 13,
            },
            {
                name: 'Banana',
                id: 14,
            },
            {
                name: 'Watermelon',
                id: 15,
            },
            {
                name: 'Kiwi fruit',
                id: 16,
            },
        ],
    },

];

function TextAreaInput(props) {
    return (
        <TextInput
            {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
            editable
            maxLength={40}
            style={{flex: 0.5, borderBottomWidth: 1, borderBottomColor: 'black', borderTopWidth: 1, borderTopColor: 'black', textAlignVertical: 'top'}}
            placeholder={"Añadir notas.."}
        />
    );
}

export default class NuevoPedido extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            dropdownOptions: [
                { value: 0, label: 'Burger' },
                { value: 1, label: 'Ensaladas' },
                { value: 2, label: 'Bebidas' },
                { value: 3, label: 'Postres' },
                { value: 4, label: 'Meriendas' },
                { value: 5, label: 'Menu especial' },
            ],
            dropdownMesas: [
                { value: 0, label: 'c23' },
                { value: 1, label: '22' },
                { value: 2, label: '333' },
                { value: 3, label: '3' },
                { value: 4, label: '111' },
                { value: 5, label: 'm23334' },
            ],
            selectedItems: [],
            valueNote: ''
        };
    }

    onSelectedItemsChange = (selectedItems) => {
        this.setState({ selectedItems });
    };


    render(){
        return(
            <View style={styles.container}>
                <View style={{flex: 0.15, flexDirection: 'row', alignItems: 'center', backgroundColor: "#ee5253"}}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={{flex: 0.5, marginLeft: 10}}
                        onPress={()=> this.props.navigation.goBack()}
                    >
                        <Icon name={"x"} size={30} color={"white"}/>

                    </TouchableOpacity>
                    <Text style={{flex: 1, color: "white", fontSize: 20, fontWeight: 'bold', textAlign: 'left'}}> Nuevo pedido </Text>
                    <TouchableOpacity
                        activeOpacity={0.4}
                        style={{flex: 0.5, alignItems: 'center'}}
                        onPress={()=> this.props.navigation.navigate("")}
                    >
                        <Icon2 name={"check"}  size={30} color={"white"}/>

                    </TouchableOpacity>
                </View>
                <SectionedMultiSelect
                    items={items}
                    uniqueKey="id"
                    subKey="children"
                    selectText="Escoge los productos.."
                    showDropDowns={true}
                    readOnlyHeadings={true}
                    onSelectedItemsChange={this.onSelectedItemsChange}
                    selectedItems={this.state.selectedItems}
                    styles={{flex: 0.1}}
                />
                <TextAreaInput
                    multiline
                    numberOfLines={10}
                    onChangeText={text => this.setState({valueNote: text})}
                    value={this.state.valueNote}
                />
                <View style={{flex: 0.5, flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={{flex: 1, fontSize: 20}}>Seleccion una mesa: </Text>
                    <SelectInput mode={'dropdown'} value={0} options={this.state.dropdownMesas} style={{flex: 0.8}}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    dropdown:{
        flex: 1
    },
});
