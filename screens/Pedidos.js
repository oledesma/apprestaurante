import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    ScrollView
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import PedidosList from './PedidosList';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Pedidos extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={{flex: 0.1, backgroundColor: "#ee5253", flexDirection: 'row', alignItems: 'center'}}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={{flex: 0.5, marginLeft: 10}}
                        onPress={()=> this.props.navigation.goBack()}
                    >
                        <Image source={require("../images/left-arrow.png")} style={{
                            resizeMode: 'stretch',
                            width: 20,
                            height: 20
                        }}/>

                    </TouchableOpacity>
                    <Text style={{flex: 1, color: "white", marginLeft: 10, fontSize: 20, fontWeight: 'bold', textAlign: 'center'}}> Pedidos </Text>

                    <TouchableOpacity
                        style={{
                            flex: 0.8,
                            flexDirection: 'row',
                            backgroundColor: '#FFE39C',
                            height: '82%',
                            justifyContent: 'center',
                            alignItems:'center',
                            marginRight: 5
                        }}
                        activeOpacity={0.8}
                    >
                        <Text style={{flex: 1, color: "white", fontSize: 20, marginLeft: 20}}>Nuevo</Text>
                        <Icon style={{flex: 0.5}} name="add"  size={30} color="#01a699" />
                    </TouchableOpacity>
                </View>
                <PedidosList/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
