import React, {Component} from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    Button,
    View,
    SafeAreaView, TouchableOpacity, Image, ImageBackground, FlatList, Dimensions, Alert, Modal,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';

const images = [
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoGluten-Gluten_icon-icons.com_67600.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoHuevo-Egg_icon-icons.com_67598.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/Fish_icon-icons.com_67594.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoLacteos-DairyProducts_icon-icons.com_67597.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/Soy_icon-icons.com_67593.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoFrutosCascaraPeelFruits_icon-icons.com_67601.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoCrustaceo-Crustaceans_icon-icons.com_67603.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoGranosSesamo-SesameGrains_icon-icons.com_67599.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoMoluscos-Mollusks_icon-icons.com_67596.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoDioxidoAzufreSulfitosSulfurDioxideSulphites_icon-icons.com_67602.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoMostaza-Mustard_icon-icons.com_67595.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoApio-Celery_icon-icons.com_67605.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoCacahuete-Peanuts_icon-icons.com_67604.png",
        seleccionado: false
    },
    {
        urlImagen:
            "https://cdn.icon-icons.com/icons2/852/PNG/128/IconoAlergenoAltramuces-Lupins_icon-icons.com_67606.png",
        seleccionado: false
    }
];
const numColumns = 3;

const formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
        data.push({ urlImagen: `blank-${numberOfElementsLastRow}`, seleccionado: false });
        numberOfElementsLastRow++;
    }

    return data;
};

const options=[
    {
        name:'Option 1',
        value: '1'
    },
    {
        name:'Option 2',
        value: '2'
    },
    {
        name:'Option 3',
        value: '3'
    },
    {
        name:'Option 4',
        value: '4'
    },
    {
        name:'Option 5',
        value: '5'
    }
];

export default class ProductDetail extends Component{
    state = {
        modalVisible: false,
        visibleModalId: null,
        imageState: images,
        newData: false,
        filePath: null,
        fileData: null,
        fileUri: null,
        selectedIndex: 0
    };

    changeBackgrounColor(index, estado){
        let cambioImages = this.state.imageState;
        cambioImages[index].seleccionado = estado;

        this.setState({imageState: cambioImages});
        this.setState({newData: true});
        setTimeout(()=>{
            this.setState({newData: false});
        }, 60);
    }

    renderModalContent = () => (
        <View style={styles.content}>
            <Text style={styles.contentTitle}>Escoger opción de imagen</Text>
            <View style={{flex: 1, flexDirection: 'row'}}>
                <Button
                    onPress={() => {this.launchCamera(); this.setState({ visibleModal: null })}}
                    title="Tomar foto"
                />
                <Button
                    onPress={() => {this.chooseImage(); this.setState({ visibleModal: null })}}
                    title="Escoger imagen de galería"
                />
            </View>
        </View>
    );


    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    renderFlatListAllergens = ({item, index}) => {
        if(!item.seleccionado) return;
      return(
          <Image style={{width: 60, height: 80, resizeMode: 'stretch'}} source={{uri: item.urlImagen}}/>
      )
    };
    chooseImage = () => {
        let options = {
            title: 'Seleccionar imagen',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            cancelButtonTitle: 'Cancelar',
            chooseFromLibraryButtonTitle: 'Escoger de galería..',
            takePhotoButtonTitle: 'Tomar foto..'
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                // alert(JSON.stringify(response));s
                console.log('response', JSON.stringify(response));
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri
                });
            }
        });
    };


    launchImageLibrary = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                const source = { uri: response.uri };
                console.log('response', JSON.stringify(response));
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri
                });
            }
        });

    };
    launchCamera = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                const source = { uri: response.uri };
                console.log('response', JSON.stringify(response));
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri
                });
            }
        });

    };

    renderItem = ({ item, index }) => {
        if (item.empty === true) {
            return <View style={[styles.item, styles.itemInvisible]} />;
        }
        return (
            <View
                style={{
                    backgroundColor: this.state.imageState[index].seleccionado ? '#58cc5e' : 'transparent',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 20,
                    flex: 1,
                    margin: 1,
                    height: Dimensions.get('window').width / 2.8
                }}
            >
                <TouchableOpacity style={{flex: 1, alignItems: 'center'}} onPress={() => this.changeBackgrounColor(index, item.seleccionado ? false : true)}>
                    <Image source={{uri: item.urlImagen}} style={{
                        width: 70,
                        height: 100,
                        resizeMode: 'stretch'

                    }}/>
                </TouchableOpacity>
            </View>
        );
    };

    render() {

        return(
            <View style={{flex: 1}}>
                <View style={{flex: 0.1, backgroundColor: '#ee5253', alignItems: 'center',  flexDirection: 'row'}}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={{flex: 0.1, marginLeft: 10}}
                    >
                        <Image source={require("../images/left-arrow.png")} style={{
                            resizeMode: 'stretch',
                            width: 20,
                            height: 20
                        }}/>

                    </TouchableOpacity>
                    <Text style={{flex: 1, color: "white", marginLeft: 80, fontSize: 15}}> Hamburguesa con queso </Text>
                </View>

                <View style={{flex: 1, width: '100%', height: '100%', flexDirection: 'column'}}>

                    <ScrollView horizontal={false}>
                        <View>
                            <ImageBackground source={{uri:"https://amp.businessinsider.com/images/5c420211b492cb5cdb1d88d4-750-501.jpg"}} style={{flex: 1, marginTop: 5, marginRight: 10, marginLeft: 10, width: 340, height: 200 }}
                            imageStyle={{borderRadius: 20}}>
                                <TouchableOpacity style={{flex: 1, alignItems: 'flex-end', marginRight: 5}} onPress={() => this.chooseImage()}>
                                    <Image source={require("../images/camera.png")} style={{resizeMode: 'stretch', width: 50, height: 50}}/>

                                </TouchableOpacity>
                            </ImageBackground>
                            <View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
                                <Text style={{flex: 1, fontSize: 24, fontFamily: 'louis_george_cafe_bold', marginLeft: 30}}>Hamburguesa con queso</Text>
                                <TouchableOpacity style={{flex: 0.1, alignItems: 'flex-start', marginRight: 20}}  onPress={() => this.customRowPicker.setModalVisible(true)} >
                                    <Image source={require("../images/edit.png")} style={{resizeMode: 'stretch', width: 30, height: 30}}/>
                                </TouchableOpacity>

                            </View>
                            <Text style={{flex: 1, color: 'green', fontFamily: 'louis_george_cafe_bold_italic', alignSelf: 'center' , fontSize: 18, marginTop: 15}}>9.40€</Text>
                            <View style={{flex: 1}}>
                                <Text style={{flex: 0.2, marginLeft: 25, fontSize: 22, fontFamily: 'louis_george_cafe_bold'}}>Alérgenos</Text>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{marginTop: 8, flex: 1}}>
                                        <View style={{flexDirection: 'row', marginLeft: 10 }}>
                                            <FlatList
                                                horizontal={true}
                                                data={this.state.imageState}
                                                extraData={this.state}
                                                renderItem={this.renderFlatListAllergens}
                                            />
                                        </View>
                                    </ScrollView>
                                    <TouchableOpacity style={{alignItems: 'flex-start', marginRight: 25, flex: 0.2}} onPress={() => {
                                        this.setModalVisible(true);
                                    }}>
                                        <Image source={require("../images/add.png")} style={{resizeMode: 'stretch', width: 50, height: 50, marginTop: 10, marginLeft: 10}}/>
                                    </TouchableOpacity>
                                    <Modal
                                        presentationStyle="pageSheet"
                                        animationType="slide"
                                        transparent={false}
                                        visible={this.state.modalVisible}>
                                        <View style={{flex: 1, width: '100%', height: '100%'}}>
                                            <View style={{flex: 0.1, alignItems: 'center',  flexDirection: 'row'}}>
                                                <TouchableOpacity
                                                    activeOpacity={0.8}
                                                    style={{flex: 0.1, marginLeft: 10}}
                                                    onPress={() => {
                                                        this.setModalVisible(!this.state.modalVisible);
                                                    }}
                                                >
                                                    <Image source={require("../images/left-arrow.png")} style={{
                                                        resizeMode: 'stretch',
                                                        width: 20,
                                                        height: 20,
                                                        tintColor: '#ee5253'
                                                    }}/>

                                                </TouchableOpacity>
                                                <TouchableOpacity style={{
                                                    flex: 1,
                                                    borderColor: "#ee5253",
                                                    borderWidth: 2,
                                                    fontSize: 15,
                                                    marginLeft: 200,
                                                    marginRight: 10,
                                                    padding: 10,
                                                    marginTop: 5,
                                                    borderRadius: 15,
                                                    flexDirection: 'row'}}
                                                    onPress={() => {
                                                      this.setModalVisible(!this.state.modalVisible);
                                                    }}
                                                >
                                                    <Text style={{flex: 1, textAlign: 'center', alignSelf: 'center', color: '#ee5253'}}>Aceptar</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{flex: 1, marginTop: 10}}>
                                                <FlatList
                                                    extraData={this.state.newData}
                                                    data={formatData(this.state.imageState, numColumns)}
                                                    style={styles.containerFlatList}
                                                    renderItem={this.renderItem}
                                                    numColumns={numColumns}
                                                />
                                            </View>
                                        </View>
                                    </Modal>
                                </View>
                            </View>
                            <View style={{flex: 1, marginTop: 10, paddingBottom: 10}}>
                                <View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
                                    <Text style={{flex: 1, marginLeft: 25, fontSize: 22, fontFamily: 'louis_george_cafe_bold'}}>Descripción</Text>
                                    <TouchableOpacity style={{flex: 0.1, alignItems: 'flex-start', marginRight: 20}}>
                                        <Image source={require("../images/edit.png")} style={{resizeMode: 'stretch', width: 30, height: 30}}/>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex: 1, marginLeft: 10, marginRight: 10, backgroundColor: '#ededed', borderRadius: 12, marginTop: 10}}>
                                    <Text style={{margin: 10, textAlign: 'center', fontFamily: 'louis_george_cafe', fontSize: 18}}>Hamburguesa con queso, que lleva pan, ajo, cebolla, etc.</Text>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerFlatList: {
        flex: 1,
        marginVertical: 20,
    },
    content: {
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20,
        borderColor: "rgba(0, 0, 0, 0.1)",
    },
    contentTitle: {
        fontSize: 20,
        marginBottom: 12
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    }
});
