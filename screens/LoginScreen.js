import React, { Component } from 'react'
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ImageBackground,
    TouchableOpacity,
    Image,
    TextInput,
    FlatList,
    ScrollView,
    KeyboardAvoidingView
} from 'react-native'


import LoginForm from "./LoginForm";

export default class App extends Component {
    render() {
        //require("./images/cookie.png")
        return (
            <ImageBackground source={require("../images/loginBackgroundImage.jpg")} style={{flex: 1}} imageStyle={{resizeMode: 'cover'}}>
                <KeyboardAvoidingView behavior="padding" style={styles.loginContainer}>
                    <View style={styles.logoContainer}>
                        <Image source={require("../images/cookie.png")}
                               style={styles.logo}
                        />
                        <Text style={styles.title}>¡Gestiona tu restauración ya!</Text>
                    </View>
                    <View style={styles.formContainer}>
                        <LoginForm/>
                    </View>
                </KeyboardAvoidingView>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    loginContainer:{
        flex: 1,
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo:{
        width: 100,
        height: 100
    },
    formContainer:{

    },
    title:{
        color: 'white',
        marginTop: 10,
        width: 180,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: '700',
        backgroundColor: '#ee5253',
        padding: 10,
        borderRadius: 10
    }
});
