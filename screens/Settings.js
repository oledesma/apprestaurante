import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Switch, ScrollView, Image} from 'react-native';
import {Avatar, Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import InputFloatingTitle from '../components/InputFloatingTitle';

export default class LoginForm extends Component{
    state = {
        value: '',
        switchValue: false
    };
    toggleSwitch = (value) => {
        //onValueChange of the switch this function will be called
        this.setState({switchValue: value})
        //state changes according to switch
        //which will result in re-render the text
    }
    render() {
        return(
            <View style={styles.container}>
                <View style={{flex: 0.1, backgroundColor: '#ee5253', alignItems: 'center',  flexDirection: 'row'}}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={{flex: 0.1, marginLeft: 10}}
                        onPress={()=> this.props.navigation.goBack()}
                    >
                        <Image source={require("../images/left-arrow.png")} style={{
                            resizeMode: 'stretch',
                            width: 20,
                            height: 20
                        }}/>

                    </TouchableOpacity>
                    <Text style={{flex: 1, color: "white", marginLeft: 100, fontSize: 20, fontWeight: 'bold'}}> Ajustes </Text>
                </View>
                <ScrollView style={{flex: 1}}>
                    <View style={{flex: 0.3, backgroundColor: '#ee5253', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', paddingTop: 20}}>
                        <Avatar
                            containerStyle={{
                                flex: 0.2,
                                borderWidth: 2,
                                borderColor: 'white',
                            }}
                            size="large"
                            rounded
                            source={require('../images/baker.png')}
                        />
                    </View>
                    <View style={{flex: 1, backgroundColor: '#ee5253'}}>
                        <Input
                            placeholder='INPUT WITH CUSTOM ICON'
                            label="Empresa"
                            labelStyle={{color: 'white', opacity: 0.8}}
                            value="Tropicalisima"
                            inputStyle={{color: 'white'}}
                            disabled='true'
                            disabledInputStyle={{color: 'white', opacity: 1}}
                        />
                        <Input
                            placeholder='INPUT WITH CUSTOM ICON'
                            label="Horario"
                            leftIcon={
                                <Icon
                                    name='hourglass-half'
                                    size={25}
                                    color='white'
                                />
                            }
                            leftIconContainerStyle={{marginLeft: 0}}
                            labelStyle={{color: 'white', opacity: 0.8}}
                            value="10:00 - 21:00"
                            inputStyle={{color: 'white'}}
                            disabled='true'
                            disabledInputStyle={{color: 'white', opacity: 1}}
                        />
                        <Input
                            placeholder='INPUT WITH CUSTOM ICON'
                            leftIcon={
                                <Icon
                                    name='street-view'
                                    size={25}
                                    color='white'
                                />
                            }
                            leftIconContainerStyle={{marginLeft: 0}}
                            label="Dirección"
                            labelStyle={{color: 'white', opacity: 0.8}}
                            value="Av. meridiana 338"
                            inputStyle={{color: 'white'}}
                            disabled='true'
                            disabledInputStyle={{color: 'white', opacity: 1}}
                        />
                        <Input
                            placeholder='INPUT WITH CUSTOM ICON'
                            leftIcon={
                                <Icon
                                    name='envelope'
                                    size={25}
                                    color='white'
                                />
                            }
                            leftIconContainerStyle={{marginLeft: 0}}
                            label="Email"
                            labelStyle={{color: 'white', opacity: 0.8}}
                            value="tropicalisima@gmail.com"
                            inputStyle={{color: 'white'}}
                            disabled='true'
                            disabledInputStyle={{color: 'white', opacity: 1}}
                        />
                        <Input
                            placeholder='INPUT WITH CUSTOM ICON'
                            leftIcon={
                                <Icon
                                    name='lock'
                                    size={25}
                                    color='white'
                                />
                            }
                            leftIconContainerStyle={{marginLeft: 0}}
                            label="Contraseña"
                            secureTextEntry={true}
                            labelStyle={{color: 'white', opacity: 0.8}}
                            value="Tropicalisima"
                            inputStyle={{color: 'white'}}
                            disabled
                            disabledInputStyle={{color: 'white', opacity: 1}}
                        />
                        <View style={{flex:2, flexDirection: 'row', justifyContent: 'center', backgroundColor: 'white', minHeight: 60, borderBottomColor: '#ee5253', borderBottomWidth: 2}}>
                            <Icon
                                name='moon-o'
                                size={25}
                                color='black'
                                style={{flex: 0.1, alignSelf: 'center', marginLeft: 10}}
                            />
                            <Text style={{flex: 1, color: '#ee5253', alignSelf: 'center', fontSize: 20}}>Modo oscuro</Text>
                            <Switch
                                style={{flex: 0.6}}
                                onValueChange = {this.toggleSwitch}
                                value = {this.state.switchValue}
                            />
                        </View>
                        <View style={{flex: 2, minHeight: 60, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'center'}}>
                            <Icon
                                name='bell'
                                size={25}
                                color='black'
                                style={{flex: 0.1, alignSelf: 'center', marginLeft: 10}}
                            />
                            <Text style={{flex: 1, color: '#ee5253', fontSize: 20, alignSelf: 'center'}}>Notificaciones</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    }
});
