import React, {Component} from 'react';
import {StyleSheet, View, ImageBackground, TextInput, TouchableOpacity, Text, StatusBar}   from "react-native";

export default class LoginForm extends Component{
    render(){
        return(
            <View style={styles.container} >
                <StatusBar
                    barStyle="light-content"
                />
                <TextInput
                    placeholder="Usuario o email"
                    placeholderTextColor="#5e5e5e"
                    returnKeyType="next"
                    onSubmitEditing={() => this.passwordInput.focus()}
                    keyboardType={"email-address"}
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={styles.input}
                />
                <TextInput
                    placeholder="contraseña"
                    placeholderTextColor="#5e5e5e"
                    secureTextEntry
                    returnKeyType="go"
                    style={styles.input}
                    ref={(input)=> this.passwordInput = input}
                />
                <TouchableOpacity style={styles.buttonContainer}>
                    <Text style={styles.buttonText} >Entrar</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container:{
    padding: 20,
   },
    input:{
       height: 40,
        backgroundColor: 'rgba(255,255,255,0.8)',
        marginBottom: 10,
        color: '#000000',
        paddingHorizontal: 10
    },
    buttonContainer:{
       backgroundColor: '#ee5253',
        paddingVertical: 15
    },
    buttonText:{
       textAlign:'center',
        fontWeight: '700',
        color: '#FFFFFF'
    }
});