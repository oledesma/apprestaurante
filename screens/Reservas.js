import React, {Component} from "react";
import {StyleSheet, Text, TouchableOpacity, View, Dimensions, ScrollView, Button, Image} from "react-native";



import ReservasList from "./ReservasList";
import { Icon } from 'react-native-elements'

export default class Reservas extends Component{
    render(){
        return(
            <View style={styles.scene}>
                <View style={{flex: 0.1, backgroundColor: "#ee5253", flexDirection: 'row', alignItems: 'center'}}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={{flex: 0.5, marginLeft: 10}}
                        onPress={()=> this.props.navigation.goBack()}
                    >
                        <Image source={require("../images/left-arrow.png")} style={{
                            resizeMode: 'stretch',
                            width: 20,
                            height: 20
                        }}/>

                    </TouchableOpacity>
                    <Text style={{flex: 1, color: "white", marginLeft: 10, fontSize: 20, fontWeight: 'bold'}}> Reservas </Text>
                </View>
                <ReservasList/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    scene:{
      flex:1,
    },
    flatListItem: {
        color: 'black',
        padding: 10,
        fontSize: 18,
        flex: 1,
        flexDirection: 'row',
        fontFamily: 'roboto'
    },
    imageIconList: {
        width: 30,
        height: 30,
        resizeMode: 'stretch'
    },
    tabBar: {
        flexDirection: 'row',
        paddingTop: 10,
        backgroundColor: "#ee5253"
    },
    tabItem: {

    },
});
/*

 */
