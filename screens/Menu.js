import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  TouchableOpacity,
  Text,
  Image,
    ScrollView
} from 'react-native';
import {Avatar} from 'react-native-elements';
import Reservas from '../screens/Reservas';
import Products from './Products';
import Settings from './Settings';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


function MenuComponent({ navigation }) {

  var nombreBar = 'Tropicalisima';
  var estadoBar = 'Abierto';
  var abierto =
      'https://www.biocenterzann.com/wp-content/uploads/2017/06/green-dot-hi-300x300.png';
  var cerrado =
      'https://www.stickpng.com/assets/images/58afdad6829958a978a4a693.png';
  return (
      <View style={{flex: 1}}>
        <ScrollView style={{flex: 1}}>
          <ImageBackground
              source={{
                uri:
                    'https://image.freepik.com/free-photo/blur-coffee-cafe-shop-restaurant-with-bokeh-background_1421-472.jpg',
              }}
              imageStyle={{width: '100%', height: 200, resizeMode: 'cover'}}
              style={styles.background}>
            <View
                style={{
                  marginTop: 50,
                  marginRight: 10,
                  marginLeft: 10,
                  flexDirection: 'row',
                }}>
              <Avatar
                  containerStyle={{
                    flex: 1.5,
                    borderWidth: 2,
                    borderColor: 'white',
                  }}
                  size="large"
                  rounded
                  source={require('../images/baker.png')}
              />
              <Text style={styles.nombreBar}>{nombreBar}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.estadoBar}>
                {estadoBar}{' '}
                <Image
                    style={{resizeMode: 'stretch', width: 15, height: 15}}
                    source={{uri: abierto}}
                />
              </Text>
            </View>
          </ImageBackground>
          <View style={styles.botonera}>
            <TouchableOpacity activeOpacity={0.5} style={styles.botones} onPress={()=>navigation.navigate('Reservas')}>
              <Image
                  source={require('../images/reserva.png')}
                  style={styles.imageIconStyle}
              />
              <View style={styles.separatorLine} />
              <Text style={styles.TextStyle}> Reservas </Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.5} style={styles.botones} onPress={()=>navigation.navigate('Products')}>
              <Image
                  source={require('../images/productsIcon.png')}
                  style={styles.imageIconStyle}
              />
              <View style={styles.separatorLine} />
              <Text style={styles.TextStyle}> Productos </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.botonera2}>
            <TouchableOpacity activeOpacity={0.5} style={styles.botones}>
              <Image
                  source={require('../images/takeOrderIcon.png')}
                  style={styles.imageIconStyle}
              />
              <View style={styles.separatorLine} />
              <Text style={styles.TextStyle}> Tomar pedido </Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.5} style={styles.botones}>
              <Image
                  source={require('../images/round-table.png')}
                  style={styles.imageIconStyle}
              />
              <View style={styles.separatorLine} />
              <Text style={styles.TextStyle}> Mesas </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.botonera2}>
            <TouchableOpacity activeOpacity={0.5} style={styles.botones}>
              <Image
                  source={require('../images/worker.png')}
                  style={styles.imageIconStyle}
              />
              <View style={styles.separatorLine} />
              <Text style={styles.TextStyle}> Control personal </Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.5} style={styles.botones} onPress={()=>navigation.navigate('Settings')}>
              <Image
                  source={require('../images/settingsIcon.png')}
                  style={styles.imageIconStyle}
              />
              <View style={styles.separatorLine} />
              <Text style={styles.TextStyle}> Ajustes </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>)
}

const Stack = createStackNavigator();

function MyStack() {
  return (
      <Stack.Navigator screenOptions={{
        headerShown: false,
      }}>
        <Stack.Screen name="Menu" component={MenuComponent}/>
        <Stack.Screen name="Reservas" component={Reservas}/>
        <Stack.Screen name="Products" component={Products}/>
        <Stack.Screen name="Settings" component={Settings}/>
      </Stack.Navigator>
  );
}

export default class Menu extends Component{
  render(){
    return(
        <NavigationContainer>
          <MyStack />
        </NavigationContainer>
    )
  }
}


const styles = StyleSheet.create({
  botonera: {
    flex: 1.5,
    marginTop: 20,
    marginRight: 10,
    marginLeft: 10,
    height: 150,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  botonera2: {
    flex: 1.5,
    marginTop: 10,
    height: 150,
    marginRight: 10,
    marginLeft: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  help: {
    flex: 1,
    marginTop: 10,
    marginRight: 10,
    marginLeft: 10,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  helpButton: {
    flex: 0.8,
    width: '20%',
    height: '80%',
  },
  backgrounHelp: {
    flex: 2,
    flexDirection: 'row',
    backgroundColor: '#ff8657',
    alignItems: 'center',
    borderRadius: 10,
  },
  textHelp: {
    flex: 1,
    fontSize: 20,
    fontFamily: 'roboto',
    color: 'white',
    marginLeft: 10,
  },
  helpImage: {
    width: '65%',
    marginLeft: 15,
    height: '100%',
    resizeMode: 'stretch',
  },
  botones: {
    flex: 1,
    backgroundColor: '#ff8657',
    margin: 5,
    borderRadius: 20,
    alignItems: 'center',
  },
  buttonsStyle: {
    flex: 4,
    marginLeft: 10,
    borderRadius: 20,
    width: 47,
  },
  background: {
    flex: 1,
  },
  estadoBar: {
    flex: 1,
    fontSize: 16,
    marginTop: 20,
    marginLeft: 17,
    color: 'white',
    textAlign: 'left',
    fontFamily: 'Roboto',
  },
  nombreBar: {
    flex: 5,
    fontSize: 28,
    marginTop: 25,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Roboto',
  },
  separatorLine: {
    backgroundColor: '#fff',
    height: 2,
    width: '45%',
  },
  imageIconStyle: {
    marginTop: 15,
    height: 55,
    width: 50,
    resizeMode: 'stretch',
  },
  TextStyle: {
    color: '#fff',
    marginTop: 3,
    textAlign: 'center',
    fontSize: 17,
  },
});
