import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    Button,
    View,
    SafeAreaView,
    TouchableOpacity, FlatList, Image,
    Dimensions, Picker,
} from 'react-native';
import SelectInput from 'react-native-select-input-ios'

const flatListData = [
    {
        "key": "1",
        "nombre": "Cielo Martinez",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
        "precio": "24€",
        "description": "Una descripción super larga",
        "unidades": 24
    },
    {
        "key": "2",
        "nombre": "Cielo 2",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
        "precio": "24€",
        "description": "Una descripción super larga",
        "unidades": 24
    },
    {
        "key": "3",
        "nombre": "Cielo Cupido",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
        "precio": "24€",
        "description": "Una descripción super larga",
        "unidades": 24
    },
    {
        "key": "4",
        "nombre": "Cielo d",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
        "precio": "24€",
        "description": "Una descripción super larga",
        "unidades": 24
    },
    {
        "key": "5",
        "nombre": "Cielo Martinez",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
        "precio": "24€",
        "description": "Una descripción super larga",
        "unidades": 24
    },
    {
        "key": "6",
        "nombre": "Cielo Martinez",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
        "precio": "24€",
        "description": "Una descripción super larga",
        "unidades": 24
    },
    {
        "key": "7",
        "nombre": "Cielo Martinez",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
        "precio": "24€",
        "description": "Una descripción super larga",
        "unidades": 24
    },
];

const formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
        data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
        numberOfElementsLastRow++;
    }

    return data;
};
const numColumns = 2;

export default class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdownOptions: [
                { value: 0, label: 'Burger' },
                { value: 1, label: 'Ensaladas' },
                { value: 2, label: 'Bebidas' },
                { value: 3, label: 'Postres' },
                { value: 4, label: 'Meriendas' },
                { value: 5, label: 'Menu especial' },
            ]
        };
    }

    renderItem = ({ item, index }) => {
        if (item.empty === true) {
            return <View style={[styles.item, styles.itemInvisible]} />;
        }
        return (
            <View
                style={styles.item}
            >
                <TouchableOpacity style={styles.containerButton}>
                    <Image source={require("../images/burgerBlank.jpg")} style={styles.imageProduct}/>
                    <View style={styles.contentProduct}>
                        <Text style={{flex: 1}}>Producto Ejemplo</Text>
                        <Text style={{flex: 0.2, color: 'green'}}>5€</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    };

    render() {
        return(
            <View style={styles.container}>
                <View style={{flex: 0.1, backgroundColor: '#ee5253', alignItems: 'center',  flexDirection: 'row'}}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={{flex: 0.1, marginLeft: 10}}
                        onPress={()=> this.props.navigation.goBack()}
                    >
                        <Image source={require("../images/left-arrow.png")} style={{
                            resizeMode: 'stretch',
                            width: 20,
                            height: 20
                        }}/>

                    </TouchableOpacity>
                    <Text style={{flex: 1, color: "white", marginLeft: 80, fontSize: 20, fontWeight: 'bold'}}> Productos </Text>
                </View>
                <View style={styles.barTop}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <SelectInput mode={'dropdown'} value={0} options={this.state.dropdownOptions} style={styles.dropdown} labelStyle={{color: 'white'}}/>
                        <TouchableOpacity
                            activeOpacity={0.5}
                            style={{flex: 0.2, marginLeft: 10, marginTop: 7}}
                        >
                            <Image source={require("../images/filtrar.png")} style={{
                                resizeMode: 'stretch',
                                width: 30,
                                height: 30,
                                tintColor: 'white'
                            }}/>

                        </TouchableOpacity>
                    </View>
                </View>
                <FlatList
                    data={formatData(flatListData, numColumns)}
                    style={styles.container}
                    renderItem={this.renderItem}
                    numColumns={numColumns}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#CCCCCC',
        zIndex: 1
    },
    barTop:{
        flex: 0.1,
        zIndex: 3,
        backgroundColor: '#ee5253'
    },
    dropdown:{
        flex: 1
    },
    item: {
        backgroundColor: 'white',
        borderRadius:10,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 10,
        height: 250, // approximate a square
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    itemText: {
        color: '#fff',
    },
    imageProduct:{
        resizeMode: 'stretch',
        width: 150,
        height: 200,
        marginTop: 8,
        flex: 1
    },
    containerButton:{
        flex: 1,
        flexDirection: 'column'
    },
    contentProduct:{
        flex: 0.4,
        alignItems: 'center',
        flexDirection: 'row'
    }
});
