import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import NuevoPedido from './NuevoPedido';

export default class FinalPedido extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{flex: 0.1, backgroundColor: '#ee5253', alignItems: 'center',  flexDirection: 'row'}}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={{flex: 0.1, marginLeft: 10}}
                        onPress={()=> this.props.navigation.goBack()}
                    >
                        <Image source={require("../images/left-arrow.png")} style={{
                            resizeMode: 'stretch',
                            width: 20,
                            height: 20
                        }}/>

                    </TouchableOpacity>
                    <Text style={{flex: 1, color: "white", marginLeft: 60, fontSize: 20, fontWeight: 'bold'}}> Finalizar pedido </Text>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
