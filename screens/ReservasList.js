import React, {Component} from "react";
import {FlatList, Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import 'react-native-gesture-handler';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { NavigationContainer } from '@react-navigation/native';
import {withBadge} from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";

const newReserva = withBadge(5)(Icon);
const Tab = createMaterialTopTabNavigator();

const reservaIcon = ({ tintColor }) => (
    <newReserva
        type="ionicon"
        name="ios-chatbubbles"
        size={24}
        color={tintColor}
    />
);

const flatListData = [
    {
        "key": "1",
        "name": "Cielo Martinez",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
    },
    {
        "key": "2",
        "name": "Cielo 2",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
    },
    {
        "key": "3",
        "name": "Cielo Cupido",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
    },
    {
        "key": "1",
        "name": "Cielo d",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
    },
    {
        "key": "1",
        "name": "Cielo Martinez",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
    },
    {
        "key": "1",
        "name": "Cielo Martinez",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png"
    },
    {
        "key": "1",
        "name": "Cielo Martinez",
        "imageUrl": "https://image.flaticon.com/icons/png/512/306/306473.png",
    },
]

function Reservas(){
    return (
        <FlatList data={flatListData}

                  renderItem={({ item, index }) => {
                      return (
                          <View elevation={5} style={{
                              flex: 1,
                              flexDirection: 'row',
                              backgroundColor: 'white',
                              marginLeft: 10,
                              borderWidth: 1,
                              borderColor: '#ee5253',
                              marginRight: 10,
                              borderRadius: 20,
                              marginTop: 10,
                              marginBottom: 10,
                              shadowColor: '#b55c04',
                              shadowOffset: { width: 0, height: 5 },
                              shadowOpacity: 2,
                              shadowRadius: 2,
                              elevation: 1,
                          }}>

                              <Image
                                  source={{ uri: item.imageUrl }}
                                  style={{ width: 50, height: 50, margin: 5, alignSelf: 'center' }}
                              />

                              <View style={{
                                  flex: 3.3,
                                  flexDirection: 'column'
                              }}>
                                  <Text style={styles.flatListItem}>{item.name}</Text>
                                  <Text style={{flex: 1, fontFamily: 'roboto', fontSize: 14, marginLeft: 10, paddingBottom: 10}}>Hoy</Text>
                              </View>
                              <View
                                  style={{
                                      flex: 0.04,
                                      height: 30,
                                      width: 1,
                                      alignSelf: 'center',
                                      backgroundColor: '#ee5253'
                                  }}
                              />
                              <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-end', marginRight: 5}}>
                                  <TouchableOpacity style={{ flex: 1, justifyContent: 'flex-end', margin: 5 }} activeOpacity={0.5}>
                                      <Image
                                          source={require("../images/checked.png")}
                                          style={styles.imageIconList}
                                      />
                                  </TouchableOpacity>
                                  <TouchableOpacity style={{ flex: 1, margin: 5 }} activeOpacity={0.5}>
                                      <Image
                                          source={require("../images/error.png")}
                                          style={styles.imageIconList}
                                      />
                                  </TouchableOpacity>
                              </View>
                          </View>
                      )
                  }}>
        </FlatList>
    )
}

function Aceptados(){
    return (
        <FlatList data={flatListData}

                  renderItem={({ item, index }) => {
                      return (
                          <View elevation={5} style={{
                              flex: 1,
                              flexDirection: 'row',
                              backgroundColor: 'white',
                              marginLeft: 10,
                              borderWidth: 1,
                              borderColor: '#ee5253',
                              marginRight: 10,
                              borderRadius: 20,
                              marginTop: 10,
                              marginBottom: 10,
                              shadowColor: '#b55c04',
                              shadowOffset: { width: 0, height: 5 },
                              shadowOpacity: 2,
                              shadowRadius: 2,
                              elevation: 1,
                          }}>

                              <Image
                                  source={{ uri: item.imageUrl }}
                                  style={{ width: 50, height: 50, margin: 5, alignSelf: 'center' }}
                              />

                              <View style={{
                                  flex: 3.3,
                                  flexDirection: 'column'
                              }}>
                                  <Text style={styles.flatListItem}>{item.name}</Text>
                                  <View style={{flex: 1, flexDirection: 'row', marginLeft: 10, paddingBottom: 10}}>
                                      <Text style={{flex: 1, fontFamily: 'roboto', fontSize: 14}}>Hoy</Text>
                                      <Text style={{flex: 1, fontFamily: 'roboto', fontSize: 14}}>Mesa: 42</Text>
                                  </View>

                              </View>
                              <View
                                  style={{
                                      flex: 0.04,
                                      height: 30,
                                      width: 1,
                                      alignSelf: 'center',
                                      backgroundColor: '#ee5253'
                                  }}
                              />
                              <View style={{ flex: 1, flexDirection: 'column', marginRight: 5}}>
                                  <TouchableOpacity style={{ flex: 1, margin: 5, alignItems: 'center'}} activeOpacity={0.5}>
                                      <Image
                                          source={require("../images/error.png")}
                                          style={{width: 30,
                                              height: 30,
                                              resizeMode: 'stretch',
                                              marginTop: 15}}
                                      />
                                  </TouchableOpacity>
                              </View>
                          </View>
                      )
                  }}>
        </FlatList>
    )
}

function Rechazados(){
    return (
        <FlatList data={flatListData}

                  renderItem={({ item, index }) => {
                      return (
                          <View elevation={5} style={{
                              flex: 1,
                              flexDirection: 'row',
                              backgroundColor: 'white',
                              marginLeft: 10,
                              borderWidth: 1,
                              borderColor: '#ee5253',
                              marginRight: 10,
                              borderRadius: 20,
                              marginTop: 10,
                              marginBottom: 10,
                              shadowColor: '#b55c04',
                              shadowOffset: { width: 0, height: 5 },
                              shadowOpacity: 2,
                              shadowRadius: 2,
                              elevation: 1,
                          }}>

                              <Image
                                  source={{ uri: item.imageUrl }}
                                  style={{ width: 50, height: 50, margin: 5, alignSelf: 'center' }}
                              />

                              <View style={{
                                  flex: 3.3,
                                  flexDirection: 'column'
                              }}>
                                  <Text style={styles.flatListItem}>{item.name}</Text>
                                  <Text style={{flex: 1, fontFamily: 'roboto', fontSize: 14, marginLeft: 10, paddingBottom: 10}}>Hoy</Text>
                              </View>

                          </View>
                      )
                  }}>
        </FlatList>
    )
}

export default class extends React.Component{
    render(){
        return(
            <Tab.Navigator>
                <Tab.Screen name="Reservas" component={Reservas} />
                <Tab.Screen name="Aceptados" component={Aceptados} />
                <Tab.Screen name="Rechazados" component={Rechazados} />
            </Tab.Navigator>

        )
    }
}

const styles = StyleSheet.create({
    scene:{
        flex:1,
    },
    flatListItem: {
        color: 'black',
        padding: 10,
        fontSize: 18,
        flex: 1,
        flexDirection: 'row',
        fontFamily: 'roboto'
    },
    imageIconList: {
        width: 30,
        height: 30,
        resizeMode: 'stretch'
    },
    tabBar: {
        flexDirection: 'row',
        paddingTop: 10,
        backgroundColor: "#ee5253"
    },
    tabItem: {

    },
});
